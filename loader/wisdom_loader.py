from torch.utils.data import Dataset
import torchvision.transforms as transforms
import pandas as pd
import os
import numpy as np
from PIL import Image
import torch
from .transforms import AddGaussianNoise, RandomErasing, SaltPepperNoise
import cv2
from torchvision.transforms import functional as F
import random
import albumentations as A
from albumentations.pytorch import ToTensorV2


class WISDOMDataset(Dataset):
    def __init__(self, dataset_path, mode="train", cfg=None, num_sample=None):
        self.mode = mode # train mode or validation mode
        '''
        self.rgb_path = os.path.join(dataset_path, "color_ims")
        self.depth_path = os.path.join(dataset_path, "depth_ims_numpy")
        self.seg_path = os.path.join(dataset_path, "modal_segmasks")
        '''
        self.rgb_path = os.path.join(dataset_path, self.mode, "rgb")
        # depth_folder = "depth_raw" if cfg["input_modality"] in "raw" else "depth_inpainted"
        depth_folder = "depth_numpy"
        self.depth_path = os.path.join(dataset_path, self.mode, depth_folder)
        self.seg_path = os.path.join(dataset_path, self.mode, "seg")

        # Read indices.npy
        # mode = "test" if mode == "val" else mode
        # indice_file = os.path.join(dataset_path, "{}_indices.npy".format(mode))
        # indices = np.load(indice_file)
        # self.rgb_list = ["image_{:06d}.png".format(idx) for idx in indices]
        # self.depth_list = ["image_{:06d}.npy".format(idx) for idx in indices]
        # self.seg_list = ["image_{:06d}.png".format(idx) for idx in indices]
        self.rgb_list = list(sorted(os.listdir(self.rgb_path)))
        self.depth_list = list(sorted(os.listdir(self.depth_path)))
        self.seg_list = list(sorted(os.listdir(self.seg_path)))

        if num_sample is not None:
            self.rgb_list = self.rgb_list[:num_sample]
            self.depth_list = self.depth_list[:num_sample]
            self.seg_list = self.seg_list[:num_sample]

        assert len(self.rgb_list) == len(self.depth_list)
        print(mode, ":", len(self.rgb_list), "images")

        self.input_modality = cfg["input_modality"]
        self.width = cfg["width"]
        self.height = cfg["height"]
        self.min_depth = 0.0
        self.max_depth = 1.0

        if mode == "train":
            self.rgb_transform = transforms.Compose([
                                        transforms.ToTensor(),
                                        transforms.Normalize(
                                        mean=[0.2900, 0.2877, 0.2976],
                                        std=[0.0849, 0.0868, 0.0794]),
            ])
            self.rgb_transform2 = A.Compose([

                                        A.OneOf([
                                            A.RandomBrightnessContrast(brightness_limit=(-0.21, 0.21), contrast_limit=(-0.16,0.16), brightness_by_max=True, always_apply=True, p=0.30),
                                            A.RGBShift(r_shift_limit=11, g_shift_limit=11, b_shift_limit=11, always_apply=False, p=0.25),
                                            A.ISONoise (color_shift=(0.01, 0.03), intensity=(0.1, 0.5), always_apply=False, p=0.05),
                                            A.GaussNoise(var_limit=(10.0, 50.0), mean=0, per_channel=True, always_apply=False, p=0.20),
                                            A.MedianBlur(blur_limit=5, always_apply=False, p=0.1),
                                            A.Sharpen(alpha=(0.2, 0.5), lightness=(0.5, 1.0), always_apply=False, p=0.1),

                                            ], p=1.0),

                                        # A.Equalize(mode='cv', by_channels=True, mask=None, mask_params=(), always_apply=False, p=0.05),
                                        # A.CLAHE (clip_limit=4.0, tile_grid_size=(8, 8), always_apply=False, p=0.05),

                                    ], p=0.975)
        else:
            self.rgb_transform = transforms.Compose([
                                        transforms.ToTensor(),
                                        transforms.Normalize(
                                        mean=[0.2900, 0.2877, 0.2976],
                                        std=[0.0849, 0.0868, 0.0794]),
            ])

    def __len__(self):
        return len(self.rgb_list)

    def __getitem__(self, idx):

        # added new augmentation constants here -------------------------------
        if self.mode == "train":
            deg_lst = [float(0), float(45)]
            deg1 = random.choices(population=deg_lst, weights=[0.70, 0.10], k=1)
            deg = deg1[0]

            prob_lst = [float(0), float(1)]
            prob1 = random.choices(population=prob_lst, weights=[0.70, 0.10], k=2)
            Hflip_prob = prob1[0]
            Vflip_prob = prob1[1]
            Hflip = transforms.RandomHorizontalFlip(p=Hflip_prob)
            Vflip = transforms.RandomVerticalFlip(p=Vflip_prob)

            crop_size_list1 = [0, self.width]
            crop_size_list2 = [100, 500]
            crop_sel = random.choices(population=[crop_size_list1, crop_size_list2], weights=[0.90, 0.10], k=1)
            crop_size = crop_sel[0]
        else:
            deg = 0.0
            Hflip_prob = 0.0
            Vflip_prob = 0.0
            Hflip = transforms.RandomHorizontalFlip(p=Hflip_prob)
            Vflip = transforms.RandomVerticalFlip(p=Vflip_prob)
            crop_size = [0, self.width]
        # ---------------------------------------------------------------------

        inputs = dict.fromkeys(["rgb", "depth", "val_mask"])
        if 'rgb' in self.input_modality:
            rgb = Image.open(os.path.join(self.rgb_path, self.rgb_list[idx])).convert("RGB")
            rgb = rgb.resize((self.width, self.height))
            # inputs["rgb"] = self.rgb_transform(rgb)
            if self.mode == "train":
                rgb = np.array(rgb).astype(np.uint8)
                # rgb = cv2.imread(rgb)
                #rgb = cv2.resize(rgb, (self.width, self.height), interpolation=cv2.INTER_NEAREST)
                # rgb = cv2.cvtColor(rgb, cv2.COLOR_BGR2RGB)
                # rgb = np.asarray(rgb, dtype='uint8')
                # rgb = np.array(rgb).astype(np.uint8)
                # print("rgb timage type =", rgb.dtype)


                # added new augmentations here-----------------------------
                rgb = self.rgb_transform2(image=rgb)
                rgb = rgb["image"]
                rgb = self.rgb_transform(rgb)
            else:
                rgb = self.rgb_transform(rgb)
            rgb = Hflip(rgb)
            rgb = Vflip(rgb)
            rgb = transforms.functional.crop(img=rgb, top=crop_size[0], left=crop_size[0], height=crop_size[1], width=crop_size[1])
            # rgb = transforms.functional.resize(img=rgb, size=[self.height, self.width])
            inputs["rgb"] = transforms.functional.rotate(rgb, deg)
            # ---------------------------------------------------------

        if 'depth' in self.input_modality:
            depth = np.load(os.path.join(self.depth_path, self.depth_list[idx])).astype(np.float32)
            # print('depth size =', depth.shape)
            depth = cv2.resize(depth, (self.width, self.height), interpolation=cv2.INTER_NEAREST)
            depth = torch.from_numpy(depth).unsqueeze(-1).permute(2, 0, 1)

            # create corresponding validity mask
            val_mask = torch.ones([self.height, self.width])
            val_mask[np.where(depth[0] == 0.0)] = 0
            val_mask = val_mask.unsqueeze(0)

            # depth clip & normalization
            depth[depth < self.min_depth] = self.min_depth
            depth[depth > self.max_depth] = self.max_depth
            depth = (depth - self.min_depth) / (self.max_depth - self.min_depth)

            # to imitate noise of raw depth map, add random erase + S&P noise
            # if "raw" in self.input_modality:
            #     depth, val_mask = RandomErasing(depth, val_mask)
            #     depth, val_mask = SaltPepperNoise(depth, val_mask)

            # added new augmentations here --------------------------
            depth = Hflip(depth)
            depth = Vflip(depth)
            depth = transforms.functional.crop(img=depth, top=crop_size[0], left=crop_size[0], height=crop_size[1], width=crop_size[1])
            # depth = transforms.functional.resize(img=depth, size=[self.height, self.width])
            depth = transforms.functional.rotate(depth, deg)
            # -----------------------------------------------------
            inputs["depth"] = torch.repeat_interleave(depth, 3, 0)
            # added new augmentations here ---------------for val mask--------
            val_mask = Hflip(val_mask)
            val_mask = Vflip(val_mask)
            val_mask = transforms.functional.crop(img=val_mask, top=crop_size[0], left=crop_size[0], height=crop_size[1], width=crop_size[1])
            # val_mask = transforms.functional.resize(img=val_mask, size=[self.height, self.width])
            val_mask = transforms.functional.rotate(val_mask, deg)
            # -----------------------------------------------------
            inputs["val_mask"] = val_mask

        if inputs["rgb"] is not None and inputs["depth"] is not None:
            img = torch.cat((inputs["rgb"], inputs["depth"]), 0)
        elif inputs["rgb"] is not None:
            img = inputs["rgb"]
        elif inputs["depth"] is not None:
            img = inputs["depth"]
        if inputs["val_mask"] is not None:
            img = torch.cat((img, inputs["val_mask"]), 0)

        seg_mask_path = os.path.join(self.seg_path, self.seg_list[idx])
        seg_mask = Image.open(seg_mask_path).convert("L")
        seg_mask = seg_mask.resize((self.width, self.height), Image.NEAREST)

        # added new augmentation here----------------------------------
        seg_mask = Hflip(seg_mask)
        seg_mask = Vflip(seg_mask)
        seg_mask = transforms.functional.crop(img=seg_mask, top=crop_size[0], left=crop_size[0], height=crop_size[1], width=crop_size[1])
        # seg_mask = transforms.functional.resize(img=seg_mask, size=[self.height, self.width])
        seg_mask = transforms.functional.rotate(seg_mask, deg)
        # -------------------------------------------------------------

        seg_mask = np.array(seg_mask)
        # instances are encoded as different colors
        obj_ids = np.unique(seg_mask)
        # first id is the background, so remove it
        obj_ids = obj_ids[1:]
        # obj_ids = obj_ids/4        # I had to put this, I missed this earlier
        # obj_ids = obj_ids.astype(int)

        # split the color-encoded mask into a set
        # of binary masks
        seg_masks = seg_mask == obj_ids[:, None, None]
        # seg_masks = (seg_mask == obj_ids[:, None, None]).astype(int)
        # get bounding box coordinates for each mask
        num_objs = len(obj_ids)
        temp_obj_ids = []
        temp_masks = []
        boxes = []
        for i in range(num_objs):
            pos = np.where(seg_masks[i])
            xmin = np.min(pos[1])
            xmax = np.max(pos[1])
            ymin = np.min(pos[0])
            ymax = np.max(pos[0])
            if int(xmax-xmin) < 1 or int(ymax-ymin) < 1 :
                continue
            temp_masks.append(seg_masks[i])
            temp_obj_ids.append(obj_ids[i])
            boxes.append([xmin, ymin, xmax, ymax])

        obj_ids = temp_obj_ids
        seg_masks = np.asarray(temp_masks)
        boxes = torch.as_tensor(boxes, dtype=torch.float32)

        # divide by four and convert to a list
        obj_ids = np.array(obj_ids)
        obj_ids = obj_ids/4        # I had to put this, I missed this earlier
        obj_ids = obj_ids.astype(int)
        obj_ids = obj_ids.tolist()
        labels = []
        for obj_id in obj_ids:
            if 1 <= obj_id:
                # labels.append(1)
                # this version merged the occluded and double occluded classes
                obj_str = str(obj_id)
                dig1 = obj_str[0]
                # dig2 = obj_str[1]
                if (dig1 == '1'):
                    # class_ID = 1, Singularized Cutting
                    labels.append(1)
                elif (dig1 == '5'):
                    # class_ID = 4, Target Cutting
                    labels.append(4)
                elif (dig1 == '3'):
                    # class_ID = 3, Occluded Cutting
                    labels.append(3)
                elif (dig1 == '4'):
                    # class_ID = 3, Double Occluded Cutting & occulated are merged
                    labels.append(3)
                else:
                    # class_ID = 2, Remains
                    labels.append(2)
            else:
                print("miss value error")
                # exit(0)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        seg_masks = torch.as_tensor(seg_masks, dtype=torch.uint8)
        image_id = torch.tensor([idx])
        if (len(boxes)) == 0:
            area = torch.as_tensor([1], dtype=torch.float32)
        else:
            area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        # suppose all instances are not crowd
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)
        target = {}
        if (len(boxes)) == 0:
            target["boxes"] = boxes
            target["labels"] = [0]
            target["masks"] = seg_masks
            target["image_id"] = image_id
            target["area"] = area
            target["iscrowd"] = iscrowd
        else:
            target["boxes"] = boxes
            target["labels"] = labels
            target["masks"] = seg_masks
            target["image_id"] = image_id
            target["area"] = area
            target["iscrowd"] = iscrowd
        return img, target
