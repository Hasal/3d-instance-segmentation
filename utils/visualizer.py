import torchvision.transforms as transforms
from torchvision.transforms import functional as F
import numpy as np
import cv2
import imgviz
import matplotlib.pyplot as plt

def draw_sample_images(loader, save_path, cfg, prefix, n_images=50):
    for i in range(n_images):
        img, target = loader.__getitem__(i)
        images = dict.fromkeys(["rgb", "depth", "val_mask"])
        if cfg["input_modality"] == "rgb": 
            images["rgb"] = img[:3, :, :]
        elif cfg["input_modality"] == "inpainted_depth": 
            images["depth"] = img[:3, :, :]
        elif cfg["input_modality"] == "raw_depth":
            images["depth"] = img[:3, :, :]
            images["val_mask"] = img[3, :, :]
        elif cfg["input_modality"] == "rgb_inpainted_depth":
            images["rgb"] = img[:3, :, :]
            images["depth"] = img[3:6, :, :]
        elif cfg["input_modality"] == "rgb_raw_depth":
            images["rgb"] = img[:3, :, :]
            images["depth"] = img[3:6, :, :]
            images["val_mask"] = img[6, :, :]

        for k in images.keys():
            if images[k] is None: continue
            if k == "rgb":
                images[k] = F.normalize(images[k],
                        # mean=[-0.485/0.229, -0.456/0.224, -0.406/0.225],
                        # std=[1/0.229, 1/0.224, 1/0.225])
                        # mean=[-0.29/0.08, -0.29/0.09, -0.30/0.08],
                        # std=[1/0.08, 1/0.09, 1/0.08])
                        mean=[-0.2900/0.0849, -0.2877/0.0868, -0.2976/0.0794],
                        std=[1/0.0849, 1/0.0868, 1/0.0794])
            img = F.to_pil_image(images[k])
            img.save("{}/{}_{}_{}.png".format(save_path, prefix, k, i))

def draw_prediction(image, pred, thresh, vis_depth=False):
    
    inv_normalize = transforms.Compose([
                        transforms.Normalize(
                            # mean = [ 0., 0., 0. ],
                            # std = [ 1/0.229, 1/0.224, 1/0.225 ]),
                            # mean = [ 0., 0., 0. ],
                            # std = [ 1/0.08, 1/0.09, 1/0.08 ]),
                            mean = [ 0., 0., 0. ],
                            std = [ 1/0.0849, 1/0.0868, 1/0.0794 ]),
                        transforms.Normalize(
                            # mean = [ -0.485, -0.456, -0.406 ],
                            # std = [ 1., 1., 1. ]),
                            # mean = [ -0.29, -0.29, -0.30 ],
                            # std = [ 1., 1., 1. ]),
                            mean = [ -0.2900, -0.2877, -0.2976 ],
                            std = [ 1., 1., 1. ]),
                        ])
    rgb = image[:3]
    rgb = inv_normalize(rgb)
    rgb = rgb.transpose(0, 2).transpose(0, 1) * 255
    rgb = np.uint8(rgb)

    scores = pred["scores"].detach().numpy()
    boxes = pred["boxes"].detach().numpy()
    masks = pred["masks"].detach().numpy()
    masks[masks >= 0.5] = 1
    masks[masks < 0.5] = 0
    cnd = scores[:] > thresh

    # add additional code to create captions to display for classes
    labels_set = pred["labels"].detach().numpy()
    # labels_set2 = np.unique(labels_set)
    labels_lst = labels_set.tolist()
    # print("pred_labels =", pred["labels"])
    # if more labels than masks (then prediction is not correct, but then)
    labels_lst_exp = list(range(len(scores[cnd])))
    label_limit = len(labels_lst_exp)
    if len(labels_lst) == len(labels_lst_exp):
        labels_lst = labels_lst
    else:
        labels_lst = labels_lst[0:label_limit]
    # print("labels set =", labels_set)
    # print("labels =", labels_lst)
    # print("masks =", masks)
    # print("masks_cnd =", masks[cnd, :, :])
    # print("mask len =", len(masks[cnd, :, :]))
    captions_set = []
    for item in labels_lst:
        # changed for merged double occluded and occluded
        if item == 1:
            captions_set.append('Singularized Cutting')
        elif item == 4:
            captions_set.append('Target Cutting')
        elif item == 3:
            captions_set.append('Occluded Cutting')
        else:
            captions_set.append('Remains')

    masks = np.array(np.squeeze(masks), dtype=np.bool)
    # instviz = imgviz.instances2rgb(image=rgb, masks=masks[cnd, :, :], labels=list(range(len(scores[cnd]))))
    # instviz = imgviz.instances2rgb(image=rgb, masks=masks[cnd, :, :], labels=[1, 2, 3, 4, 5], captions=['Singularized Cutting', 'Remains', 'Occluded Cutting', 'Double Occluded Cutting', 'Target Cutting'])
    instviz = imgviz.instances2rgb(image=rgb, masks=masks[cnd, :, :], labels=labels_lst, captions=captions_set)
    plt.figure(dpi=200)
    plt.imshow(instviz)
    plt.axis("off")
    instviz = imgviz.io.pyplot_to_numpy()
    instviz = cv2.cvtColor(cv2.resize(instviz, (rgb.shape[1], rgb.shape[0])), cv2.COLOR_BGR2RGB)
    instviz = np.hstack((rgb, instviz))

    if vis_depth:
        # add depth image
        depth = image[3:6]
        depth = depth.transpose(0, 2).transpose(0, 1) * 255
        depth = np.uint8(depth)
        instviz = np.hstack((instviz, depth))

    return instviz
