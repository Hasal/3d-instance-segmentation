# Modify cocoapi/PythonAPI/pycocotools/cocoeval.py to calculate AP for each category
import numpy as np
from pycocotools.cocoeval import COCOeval


class coco_eval(COCOeval):
    # summarize method in the COCOeval class is modified with the class labels
    def summarize(self):
        '''
        Compute and display summary metrics for evaluation results.
        Note this functin can *only* be applied on the default parameter setting
        '''
        def _summarize(ap=1, iouThr=None, areaRng='all', maxDets=100):
            p = self.params
            iStr = ' {:<18} {} @[ IoU={:<9} | area={:>6s} | maxDets={:>3d} ] = {:0.3f}'
            titleStr = 'Average Precision' if ap == 1 else 'Average Recall'
            typeStr = '(AP)' if ap == 1 else '(AR)'
            iouStr = '{:0.2f}:{:0.2f}'.format(p.iouThrs[0], p.iouThrs[-1]) \
                if iouThr is None else '{:0.2f}'.format(iouThr)

            aind = [i for i, aRng in enumerate(p.areaRngLbl) if aRng == areaRng]
            mind = [i for i, mDet in enumerate(p.maxDets) if mDet == maxDets]
            if ap == 1:
                # dimension of precision: [TxRxKxAxM]
                s = self.eval['precision']
                # IoU
                if iouThr is not None:
                    t = np.where(iouThr == p.iouThrs)[0]
                    s = s[t]
                s = s[:, :, :, aind, mind]
            else:
                # dimension of recall: [TxKxAxM]
                s = self.eval['recall']
                if iouThr is not None:
                    t = np.where(iouThr == p.iouThrs)[0]
                    s = s[t]
                s = s[:, :, aind, mind]
            if len(s[s > -1]) == 0:
                mean_s = -1
            else:
                mean_s = np.mean(s[s > -1])

                # cacluate AP(average precision) for each category, edit more

                num_classes = 4
                avg_ap = 0.0
                actual_cls = 0
                cls_labels = ['Singularized Cutting', 'Remains', 'Occluded Cutting', 'Target Cutting']
                if ap == 1:
                    print(' ')
                    print('----------------------------------------------')
                    for x1 in range(0, num_classes):
                        print('Class {} : ({})'.format(x1+1, cls_labels[x1]))

                    print(' ')

                    for i in range(0, num_classes):
                        x_mat = np.mean(s[:, :, i, :])
                        if x_mat == -1:
                            y_mat = 0
                            z_mat = None
                        else:
                            y_mat = x_mat
                            z_mat = x_mat
                            actual_cls += 1
                        print('Class {} : {}'.format(i+1, z_mat))
                        avg_ap += y_mat
                    print('(All Classes) mAP : {}'.format(avg_ap / actual_cls))
                    print(' ')

            print(iStr.format(titleStr, typeStr, iouStr, areaRng, maxDets, mean_s))
            return mean_s
