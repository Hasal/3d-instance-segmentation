# The module used to create offline data augmentation - image rotations
# pip install Pillow if you don't already have it

# import image utilities
from PIL import Image
import glob
# import os utilities


# define a function that rotates images in the current directory
# given the rotation in degrees as a parameter
def rotateImages(rotationAmt):
    i=1;
    images = glob.glob("*.png")
    # for each image in the current directory
    for image in images:
        # open the image
        img = Image.open(image)
        # rotate and save the image with the same filename
        # img.rotate(rotationAmt).save(image)
        img_rotated = img.rotate(rotationAmt)
        img_rotated.save("new/stn1_pkgxxx90_"+str(i)+".png")

        # close the image
        img.close()
        i = i+1


# examples of use
rotateImages(90)
# rotateImages(-90)
