# The module used to convert the depth images into numpy arrays

import cv2
import glob
import numpy as np
from PIL import Image


files = glob.glob("*.png")
for myFile in files:
    img = Image.open(myFile).convert("L")
    # img = cv2.imread(myFile)
    # img2 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    data = np.array(img)
    data2 = np.true_divide(data, 255)
    data3 = data[:, :, np.newaxis]
    st_myfile = str(myFile)
    st_myfile2 = st_myfile[0:-4]
    np.save('{}.npy'.format(st_myfile2), data3)
    print('{}'.format(st_myfile2))


'''
# print('X_data shape:', np.array(X_data).shape)
# x = np.load('stn1_pkg002_0_0003_uqe_dep.npy')
# print("x =", x)
# print("x1 shape =", x.shape)
y = np.load('stn1_pkg002_0_0003_uqe_im.npy')
print("y =", y)
print("y shape =", y.shape)
'''

# image_file = image_file.convert('1') # convert image to black and white
# image_file= image_file.convert('L') # convert image to monochrome
